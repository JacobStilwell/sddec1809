package sddec1809.hiddenguardian.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.lang.*;

import java.util.ArrayList;

import sddec1809.hiddenguardian.Models.SessionMessages;
import sddec1809.hiddenguardian.R;


public class GamingSessionPage extends AppCompatActivity {
    ListView listview;
    ArrayList<SessionMessages> sessionMessageList;
    int session_ID;
    String userid, machineid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gaming_session);
        listview = (ListView) findViewById(R.id.list_2);
        Toolbar myToolbar = findViewById(R.id.my_toolbar2);
        setSupportActionBar(myToolbar);
        Bundle b = getIntent().getExtras();
        session_ID = b.getInt("session_id");
        userid = b.getString("userid");
        machineid = b.getString("machineid");
        displayData();

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("onitemtag", "onitemclick");
                gotoGamingEntry(id);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater mMenuInflater = getMenuInflater();
        mMenuInflater.inflate(R.menu.hg_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.logout){
            Intent click = new Intent(this,MainActivity.class);
            startActivity(click);
        }
        if(item.getItemId() == R.id.add_device){
            Intent click = new Intent(this,AddDevice.class);
            Bundle b = getIntent().getExtras();
            String username = b.getString("userid");
            click.putExtra("userid", username);
            startActivity(click);
        }
        if(item.getItemId() == R.id.switch_device){
            Intent click = new Intent(this,ShowDevices.class);
            Bundle b = getIntent().getExtras();
            String username = b.getString("userid");
            click.putExtra("userid", username);
            startActivity(click);
        }
        return true;
    }


    protected void gotoGamingEntry(long id){
        Intent click = new Intent(this,GamingSession_Entry.class);
        click.putExtra("machine_id", machineid);
        click.putExtra("session_id", session_ID);
        click.putExtra("message_id", sessionMessageList.get((int) id).getMessageid());
        click.putExtra("user_id", userid);
        startActivity(click);
    }

    protected void displayData(){

        ConnectionClass conn = new ConnectionClass();
        try {
            sessionMessageList = conn.getSessionMessages(session_ID, machineid, userid);
        }
        catch(Exception ex){
            //handle javasqlexception
        }
        ArrayAdapter<SessionMessages> adapter = new ArrayAdapter<SessionMessages>(this,
                android.R.layout.simple_list_item_1, sessionMessageList);
        listview = (ListView) findViewById(R.id.list_2);
        listview.setAdapter(adapter);
    }

    protected void onResume() {
        super.onResume();
        //TODO - assign a list of all the gaming entries retrieved from database to gamingEntryList

        //TODO: delete events that occur seven days prior to the current date

        ConnectionClass conn = new ConnectionClass();
        try {
            Bundle b = getIntent().getExtras();
            session_ID = b.getInt("session_id");
            userid = b.getString("userid");
            sessionMessageList = conn.getSessionMessages(session_ID, machineid, userid);
        }
        catch(Exception ex){
            //handle javasqlexception
        }
        ArrayAdapter<SessionMessages> adapter = new ArrayAdapter<SessionMessages>(this,
                android.R.layout.simple_list_item_1, sessionMessageList);
        listview = (ListView) findViewById(R.id.list_2);
        listview.setAdapter(adapter);
    }


}
