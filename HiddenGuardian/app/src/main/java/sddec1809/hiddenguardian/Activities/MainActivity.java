package sddec1809.hiddenguardian.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import sddec1809.hiddenguardian.R;

public class MainActivity extends Activity
{
    ConnectionClass connectionClass;
    EditText username,user_password;
    Button btnlogin,btnregister;
    ProgressBar pbbar;
    String machineid;
    boolean mult_machineid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connectionClass = new ConnectionClass();
        username = (EditText) findViewById(R.id.editText_Username);
        user_password = (EditText) findViewById(R.id.editText_Password);
        user_password.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        btnlogin = (Button) findViewById(R.id.button_Login);
        btnregister = (Button) findViewById(R.id.button_register);
        pbbar = (ProgressBar) findViewById(R.id.progressBar);
        pbbar.setVisibility(View.GONE);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoLogin  doLogin = new DoLogin();
                doLogin.execute("");
            }
        });

        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Register.class));
            }
        });

    }

    public class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod{
        @Override
        public CharSequence getTransformation(CharSequence source, View view){
            return new PasswordCharSequence(source);
        }
        private class PasswordCharSequence implements CharSequence{
            private CharSequence mSource;
            public PasswordCharSequence(CharSequence source){
                mSource = source;
            }
            public char charAt(int index){
                return '*';
            }
            public int length(){
                return mSource.length();
            }
            public CharSequence subSequence(int start, int end){
                return mSource.subSequence(start, end);
            }
        }
    }


    public class DoLogin extends AsyncTask<String,String,String>
    {
        String z = "";
        Boolean isSuccess = false;


        String userid = username.getText().toString();
        String password = user_password.getText().toString();


        @Override
        protected void onPreExecute() {
            pbbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String r) {
            pbbar.setVisibility(View.GONE);
            Toast.makeText(MainActivity.this,r,Toast.LENGTH_SHORT).show();

            if(isSuccess) {
                Toast.makeText(MainActivity.this,r,Toast.LENGTH_SHORT).show();
                if (mult_machineid == false){
                    Intent intent = new Intent(MainActivity.this, MainPage.class);
                    intent.putExtra("userid", userid);
                    getMachineID();
                    intent.putExtra("machineid", machineid);
                    startActivity(intent);
                }
                else {
                    Intent intent = new Intent(MainActivity.this, ShowDevices.class);
                    intent.putExtra("userid", userid);
                    startActivity(intent);
                }

            }

        }

        protected void getMachineID() {
            try {
                Connection con = connectionClass.CONN();
                if (con == null) {
                    System.out.println("error in connection with sql server");
                } else {
                    String query = "select * from Machine where userid='" + userid + "'";
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery(query);

                    if (rs.next()) {
                        machineid = rs.getString("machineID");
                    }
                }
            } catch (Exception ex) {
                //catches exception
            }
        }

        @Override
        protected String doInBackground(String... params) {
            if(userid.trim().equals("")|| password.trim().equals(""))
                z = "Please enter User Id and Password";
            else
            {
                try {
                    Connection con = connectionClass.CONN();
                    if (con == null) {
                        z = "Error in connection with SQL server";
                        System.out.println("error in connection with sql server");
                    } else {
                        String query = "select * from ActiveDirectory where userid='" + userid + "' and pass='" + password + "'";
                        Statement stmt = con.createStatement();
                        ResultSet rs = stmt.executeQuery(query);

                        if (rs.next())
                            z = "Login successfull";
                        String query2 = "select COUNT(*) from Machine where userID='" + userid + "';";
                        Statement stmt2 = con.createStatement();
                        ResultSet rs2 = stmt2.executeQuery(query2);
                        if (rs2.next()) {
                            int count = rs2.getInt(1);
                            if (count == 0) {
                                machineid = "null";
                                mult_machineid = false;
                            } else if (count == 1) {
                                mult_machineid = false;
                            } else {
                                mult_machineid = true;
                            }
                            isSuccess = true;
                        }

                        else {
                            z = "Invalid Credentials";
                            isSuccess = false;
                        }
                    }
                    con.close();
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    z = "Exceptions";
                }
            }
            return z;
        }
    }
}