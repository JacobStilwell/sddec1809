package sddec1809.hiddenguardian.Activities;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import sddec1809.hiddenguardian.R;


public class AddDevice extends Activity {

    ConnectionClass connectionClass;
    EditText editdevicecode, editdevicename;
    Button btnconnect;
    TextView skip;
    String userid;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_device);
        connectionClass = new ConnectionClass();
        btnconnect = (Button) findViewById(R.id.button_connect);
        editdevicecode = (EditText) findViewById(R.id.edit_devicecode);
        editdevicename = (EditText) findViewById(R.id.edit_devicename);
        skip = (TextView) findViewById(R.id.skip);
        Bundle b = getIntent().getExtras();
        userid = b.getString("userid");

        btnconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddMachine addmachine = new AddMachine();
                addmachine.execute("");
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddDevice.this, MainPage.class);
                intent.putExtra("userid", userid);
                intent.putExtra("machineid", "null");
                startActivity(intent);
            }
        });

    }

    public class AddMachine extends AsyncTask<String,String,String>
    {
        String z = "";
        Boolean isSuccess = false;
        Connection con;

        String machineid = editdevicecode.getText().toString();
        String machinename = editdevicename.getText().toString();


        @Override
        protected void onPostExecute(String r) {
            Toast.makeText(AddDevice.this,r,Toast.LENGTH_SHORT).show();

            if(isSuccess) {
                Toast.makeText(AddDevice.this,r,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(AddDevice.this, MainPage.class);
                intent.putExtra("userid", userid);
                intent.putExtra("machineid", machineid);
                startActivity(intent);
            }

        }

        @Override
        protected String doInBackground(String... params) {
            if(machineid.trim().equals("")|| machinename.trim().equals(""))
                z = "Please enter Device code and Device name";
            else
            {
                try {
                    con = connectionClass.CONN();
                    if (con == null) {
                        z = "Error in connection with SQL server";
                    } else {
                        String query = "SELECT * FROM Machine WHERE MachineID='" + machineid + "';";
                        Statement stmt = con.createStatement();
                        ResultSet rs = stmt.executeQuery(query);

                        if(rs.next())
                        {
                            String username = rs.getString("userid");
                            if (username != "null"){
                                z = "This device is already in use";
                                isSuccess=false;
                            }
                            else {
                                String query2 = "UPDATE Machine SET UserID = '" + userid + "', MachineName = '" + machinename + "' WHERE MachineID = '" + machineid + "';";
                                Statement stmt1 = con.createStatement();
                                stmt1.execute(query2);
                                isSuccess = true;
                            }
                        }
                        else
                        {
                            z = "This device code is invalid";
                            isSuccess = false;
                        }

                    }
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    z = "Exceptions";

                    Log.e("hiddenguardian", "exception", ex);
                }
            }
            return z;
        }
    }
}
