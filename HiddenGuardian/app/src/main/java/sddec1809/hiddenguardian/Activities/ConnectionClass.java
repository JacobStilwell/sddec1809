package sddec1809.hiddenguardian.Activities;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import android.util.Log;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import sddec1809.hiddenguardian.Models.GamingEntry;
import sddec1809.hiddenguardian.Models.SessionMessages;


public class ConnectionClass {
    String ip = "10.24.85.81:59961";
    String classs = "net.sourceforge.jtds.jdbc.Driver";
    String db = "master";
    String un = "sa";
    String password = "databaseaccountsecure";


    @SuppressLint("NewApi")
    public Connection CONN() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        String ConnURL = null;
        try {

            Class.forName(classs);
            ConnURL = "jdbc:jtds:sqlserver://" + ip +
                    ";databaseName=" + db + ";user="
                    + un + ";password=" + password + ";sendStringParametersAsUnicode=false;prepareSQL=1;";
            conn = DriverManager.getConnection(ConnURL);
        } catch (SQLException se) {
            Log.e("ERRO", se.getMessage());
        } catch (ClassNotFoundException e) {
            Log.e("ERRO", e.getMessage());
        } catch (Exception e) {
            Log.e("ERRO", e.getMessage());
        }
        return conn;
    }

    public ArrayList<GamingEntry> UpdateDB(String machineid){
        String z;
        ConnectionClass connectionClass = this;
        Connection con = connectionClass.CONN();
        ArrayList<GamingEntry> ge_list = new ArrayList<GamingEntry>();
        if (con == null) {
            z = "Error in connection with SQL server";
            System.out.println("error in connection with sql server");
        } else {
            try {
            String query = "SELECT * FROM Messages WHERE machineid = '" + machineid + "';";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                        final GamingEntry ge = new GamingEntry();
                        ge.setuserid(rs.getString("userid"));
                        ge.setmachineid(rs.getString("machineid"));
                        ge.setDate(rs.getString("date"));
                        ge.setMessage(rs.getString("message"));
                        ge.setsessionid(rs.getInt("sessionid"));
                        String key_words = ge.getKeywords();
                        ge.setKeywords(key_words);
                        ge.setConfidence_rating(rs.getString("confidence"));
                        String[] keywords_triggered = ge.checkMessage();
                        ge.setTitle(ge.getdate(), ge.getKeywordnumber());
                        ge_list.add(ge);

                }
                con.close();
            } catch (SQLException se) {
                System.out.println("Exception: " + se);
            }
            catch (Exception e){
                System.out.println("Exception: " + e);
            }
        }
        return ge_list;
    }

    public String getKeywords(String userid, String message){
        ConnectionClass connectionClass = new ConnectionClass();
        Connection con = connectionClass.CONN();
        String keywords = "";
        if (this == null) {
            //throw an error exception
        } else {
            try {
                String query = "SELECT word FROM Keywords WHERE userid = '" + userid + "';";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    keywords += rs.getString("word") + ",";
                }
                con.close();
            } catch (Exception ex) {
                //exception
            }
        }
        return keywords;
    }

    public ArrayList<String> getKeywords(String userid){
        ConnectionClass connectionClass = new ConnectionClass();
        Connection con = connectionClass.CONN();
        ArrayList<String> keywords = new ArrayList<>();
        if (this == null) {
            //throw an error exception
        } else {
            try {
                String query = "SELECT word FROM Keywords WHERE userid = '" + userid + "';";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    String s= rs.getString("word");
                    keywords.add(s);
                }
                con.close();
            } catch (Exception ex) {
                //exception
            }
        }
        return keywords;
    }

    public ArrayList<SessionMessages> getSessionMessages(int session_id, String machineid, String userid) {
        ConnectionClass connectionClass = new ConnectionClass();
        Connection con = connectionClass.CONN();
        ArrayList<SessionMessages> sessionmessage_list = new ArrayList<SessionMessages>();
        if (this == null) {
            //error
        } else {
            try {
                String query = "SELECT * FROM Messages WHERE sessionid = '" + session_id + "' AND machineid = '" + machineid + "';";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    if (rs.getInt("sessionid") == session_id) {
                        final SessionMessages sm = new SessionMessages();
                        sm.setMessage(rs.getString("message"));
                        sm.setMessageID(rs.getInt("messageid"));
                        sm.setKeywords(this.getKeywords(userid, sm.getMessage()));
                        String timestamp = rs.getString("timestamp");
                        String keywords_triggered = sm.checkMessage();
                        int lastcommaindex = keywords_triggered.lastIndexOf(",");
                        if (keywords_triggered != "") {
                            String kt = keywords_triggered.substring(0, lastcommaindex);
                            sm.setTimeStamp(timestamp);
                            sm.setTitle(kt);
                            sessionmessage_list.add(sm);
                        }
                    }

                }
                con.close();
            } catch (SQLException se) {
                System.out.println("Exception: " + se);
            }
            catch (Exception e){
                System.out.println("Exception: " + e);
            }

        }
        return sessionmessage_list;
    }

    public ArrayList<String> getDevices(String userid) {
        ConnectionClass connectionClass = new ConnectionClass();
        Connection con = connectionClass.CONN();
        ArrayList<String> device_list = new ArrayList<String>();
        if (this == null) {
            //error
        } else {
            try {
                String query = "SELECT * FROM Machine WHERE userID = '" + userid + "';";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    device_list.add(rs.getString("MachineName"));
                }
                con.close();
            } catch (Exception e) {
                //handle exception
            }
        }
        return device_list;
    }

    public String getDeviceID(String userid, String machinename) {
        ConnectionClass connectionClass = new ConnectionClass();
        Connection con = connectionClass.CONN();
        String machineid = "";
        if (this == null) {
            //error
        } else {
            try {
                String query = "SELECT * FROM Machine WHERE userID = '" + userid + "' and MachineName = '" + machinename + "';";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    machineid = rs.getString("MachineID");
                }
                con.close();
            } catch (Exception e) {

            }
        }
        return machineid;
    }
    public ArrayList<String> getData(String machineId, int sessionId, int messageId) {
        ConnectionClass connectionClass = new ConnectionClass();
        Connection con = connectionClass.CONN();
        ArrayList<String> data = new ArrayList<String>(4);
        if (this == null) {
            //error
        } else {
            try {

                String query = "SELECT date, message, timestamp, confidence from MESSAGES WHERE machineid='" + machineId + "' and sessionid='" + sessionId + "' and messageid='" + messageId + "';";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    data.add(rs.getString(1));
                    data.add(rs.getString(2));
                    data.add(rs.getString(3));
                    data.add(rs.getString(4));
                }
                con.close();
//                String ts = rs.getString("timestamp");
//                String msg = rs.getString("message");
//                String date = rs.getString("date");
//                String conf = rs.getString("confidence");
//                data.add(0, ts);
//                data.add(1, msg);
//                data.add(2, date);
//                data.add(3, conf);
            } catch (Exception e) {
                Exception a = e;
                Log.d("Exception", e.toString());
            }
        }
        return data;
    }
}