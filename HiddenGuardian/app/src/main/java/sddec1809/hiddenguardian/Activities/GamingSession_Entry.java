package sddec1809.hiddenguardian.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import android.text.Layout;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import sddec1809.hiddenguardian.R;

public class GamingSession_Entry extends AppCompatActivity {
    private GestureDetector mDetector;
    ConnectionClass connectionClass;
    TextView textTime, textMessage;
    int session_ID;
    String machine_ID;
    String user_ID;
    int message_ID;
    ArrayList<String> data;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.entry_digest);

        Toolbar myToolbar = findViewById(R.id.my_toolbar3);
        setSupportActionBar(myToolbar);

        View myView= findViewById(R.id.myLayout);

        textTime =  findViewById(R.id.text_Time);
        textMessage= findViewById(R.id.text_Message);
        Bundle b = getIntent().getExtras();
        session_ID = b.getInt("session_id");
        machine_ID = b.getString("machine_id");
        message_ID = b.getInt("message_id");
        user_ID = b.getString("user_id");
        //textview.setText("userid: " + user_ID + "\nsession id: " + String.valueOf(session_ID) +"\nmessage id: " + String.valueOf(message_ID));
    //      populateData();
        populateData(2);
        textTime.setText(data.get(0)+"  "+ data.get(2));
        String message = data.get(1);
        String conf = data.get(3);
        String[] confArray = conf.split(",");
        String[] msgArray = message.split(" ");
        String mediumconf = "";
        String lowconf = "";
        String highconf = "";
        String[] outputArray = new String[msgArray.length];
        // get the gesture detector
        mDetector = new GestureDetector(this, new MyGestureListener());

        // Add a touch listener to the view
        // The touch listener passes all its events on to the gesture detector
        myView.setOnTouchListener(touchListener);
        myView.setClickable(true);

    }
    public SpannableStringBuilder getBuilt(){
        SpannableStringBuilder builder = new SpannableStringBuilder();
        String message = data.get(1);
        String conf = data.get(3);
        String[] confArray = conf.split(",");
        String[] msgArray = message.split(" ");
        ArrayList<String> badWords = getBadWords(user_ID);
        int i = 0;
        while(i < confArray.length){
            if (Double.valueOf(confArray[i]) > .90){
                SpannableString str1= new SpannableString(msgArray[i]+" "); //this is wrong
                str1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, str1.length(), 0);
                if(badWords.contains(str1.toString().trim())) {
                    str1.setSpan(new StyleSpan(Typeface.BOLD), 0, str1.length(), 0);
                }
                builder.append(str1);
            }
            else if(Double.valueOf(confArray[i]) < .90 && Double.valueOf(confArray[i]) > .70){
                SpannableString str1= new SpannableString(msgArray[i]+" "); //this is wrong
                str1.setSpan(new ForegroundColorSpan(Color.parseColor("#FFA500")), 0, str1.length(), 0);
                if(badWords.contains(str1.toString().trim())) {
                    str1.setSpan(new StyleSpan(Typeface.BOLD), 0, str1.length(), 0);
                }
                builder.append(str1);
            }
            else {
                SpannableString str1= new SpannableString(msgArray[i]+" "); //this is wrong
                str1.setSpan(new ForegroundColorSpan(Color.RED), 0, str1.length(), 0);
                if(badWords.contains(str1.toString().trim())) {
                    str1.setSpan(new StyleSpan(Typeface.BOLD), 0, str1.length(), 0);
                }
                builder.append(str1);
            }
            i++;
        }
        return builder;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater mMenuInflater = getMenuInflater();
        mMenuInflater.inflate(R.menu.hg_menu, menu);
        return true;
    }
    public ArrayList<String> getBadWords(String user_ID){
        ConnectionClass connectionClass = new ConnectionClass();
        ArrayList<String> badwords = connectionClass.getKeywords(user_ID);
        return badwords;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.logout){
            Intent click = new Intent(this,MainActivity.class);
            startActivity(click);
        }
        if(item.getItemId() == R.id.add_device){
            Intent click = new Intent(this,AddDevice.class);
            Bundle b = getIntent().getExtras();
            String username = b.getString("user_id");
            click.putExtra("userid", username);
            startActivity(click);
        }
        if(item.getItemId() == R.id.switch_device){
            Intent click = new Intent(this,ShowDevices.class);
            Bundle b = getIntent().getExtras();
            String username = b.getString("userid");
            click.putExtra("userid", username);
            startActivity(click);
        }
        return true;
    }
    protected  void populateData(int move){// 1:left 2:stay 3:right
        ConnectionClass conn = new ConnectionClass();
        if(move==1){
            try {
                data = conn.getData(machine_ID,session_ID,message_ID-1);
                message_ID=message_ID-1;
                textTime.setText(data.get(0)+"  "+ data.get(2));
                SpannableStringBuilder builder = getBuilt();
                textMessage.setText(builder);

                Log.d("Left","lateData: 213");
            }
            catch(Exception ex){
                //handle javasqlexception
            }
        }else if(move==2){
            try {
                data = conn.getData(machine_ID,session_ID,message_ID);
                textTime.setText(data.get(0)+"  "+ data.get(2));
                SpannableStringBuilder builder = getBuilt();
                textMessage.setText(builder);
            }
            catch(Exception ex){
                //handle javasqlexception
            }
        }else{
            try {
                data = conn.getData(machine_ID,session_ID,message_ID+1);
                message_ID=message_ID+1;
                textTime.setText(data.get(0)+"  "+ data.get(2));
                SpannableStringBuilder builder = getBuilt();
                textMessage.setText(builder);
            }
            catch(Exception ex){
                //handle javasqlexception
            }
        }
    }
    // This touch listener passes everything on to the gesture detector.
    // That saves us the trouble of interpreting the raw touch events
    // ourselves.
    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            // pass the events to the gesture detector
            // a return value of true means the detector is handling it
            // a return value of false means the detector didn't
            // recognize the event
            return mDetector.onTouchEvent(event);

        }
    };
    public void onSwipeLeft() {
        populateData(3);
    }

    public void onSwipeRight() {
        populateData(1);
    }
    // In the SimpleOnGestureListener subclass you should override
    // onDown and any other gesture that you want to detect.
    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_DISTANCE_THRESHOLD = 50;
        private static final int SWIPE_VELOCITY_THRESHOLD = 50;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float velocityX, float velocityY) {
            float distanceX = e2.getX() - e1.getX();
            float distanceY = e2.getY() - e1.getY();
            if (Math.abs(distanceX) > Math.abs(distanceY) && Math.abs(distanceX) > SWIPE_DISTANCE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                if (distanceX > 0) {
                    Log.d("TAG", "onRight: ");
                    onSwipeRight();
                }
                else{
                    Log.d("TAG", "onLeft: ");
                    onSwipeLeft();
                }
                return true;
            }
            return false;
        }
    }
}