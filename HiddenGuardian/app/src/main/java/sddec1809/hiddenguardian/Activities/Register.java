package sddec1809.hiddenguardian.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import sddec1809.hiddenguardian.R;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class Register extends Activity {

    ConnectionClass connectionClass;
    EditText regUser,regPass1;
    Button regButton, regBack;
    ProgressBar regBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        connectionClass = new ConnectionClass();
        regUser= (EditText) findViewById(R.id.registerName);
        regPass1=(EditText) findViewById(R.id.registerPass1);
        regButton=(Button) findViewById(R.id.button_Check);
        regBack= (Button) findViewById(R.id.button_Back);
        regBar= (ProgressBar) findViewById(R.id.regBar);

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoRegister doRegister = new DoRegister();
                doRegister.execute("");
            }
        });
        regBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Register.this, MainActivity.class));
            }
        });
    }


    public class DoRegister extends AsyncTask<String,String,String>
    {
        String z = "";
        Boolean isSuccess = false;
        Connection con;


        String userid = regUser.getText().toString();
        String password = regPass1.getText().toString();


        @Override
        protected void onPreExecute() {
            regBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String r) {
            regBar.setVisibility(View.GONE);
            Toast.makeText(Register.this,r,Toast.LENGTH_SHORT).show();

            if(isSuccess) {
                Toast.makeText(Register.this,r,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Register.this, AddDevice.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
            }

        }

        @Override
        protected String doInBackground(String... params) {

            if(userid.trim().equals("")|| password.trim().equals(""))
                z = "Please enter a new User Id and Password";
            else
            {
                try {
                    con = connectionClass.CONN();
                    if (con == null) {
                        z = "Error in connection with SQL server";
                    } else {
                        String query = "select * from ActiveDirectory where userid='" + userid+"';";
                        Statement stmt = con.createStatement();
                        ResultSet rs = stmt.executeQuery(query);

                        if(rs.next())
                        {
                            z = "That username is taken";
                            isSuccess=false;
                        }
                        else
                        {
                            try {
                                con = connectionClass.CONN();
                                if (con == null) {
                                    z = "Error in connection with SQL server";
                                } else {

                                    String query2 = "INSERT INTO ActiveDirectory  values ( '"+userid+"', '"+password+"');";
                                    Statement stmt1= con.createStatement();
                                    stmt1.execute(query2);
                                    isSuccess=true;
                                }
                            }
                            catch (Exception ex)
                            {
                                isSuccess = false;
                                z = "Exceptions";

                                Log.e("hiddenguardian", "exception", ex);
                            }
                        }
                    }
                    con.close();
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    z = "Exceptions";

                    Log.e("hiddenguardian", "exception", ex);
                }
            }
            return z;
        }
    }
}
