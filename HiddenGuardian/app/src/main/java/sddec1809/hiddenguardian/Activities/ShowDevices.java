package sddec1809.hiddenguardian.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sddec1809.hiddenguardian.R;

public class ShowDevices extends Activity {
    String userid, machineID;
    ArrayList<String> StringList;
    ListView listview;
    TextView adddevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_devices);
        Bundle b = getIntent().getExtras();
        userid = b.getString("userid");
        //machineID = b.getString("machineid");
        listview = findViewById(R.id.device_list);
        adddevice = findViewById(R.id.adddevice_text);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("onitemtag", "onitemclick");
                updateMachineID(id);
            }
        });

        adddevice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShowDevices.this, AddDevice.class);
                intent.putExtra("userid", userid);
                startActivity(intent);
            }
        });
    }

    protected void updateMachineID(long id){
        Intent click = new Intent(this, MainPage.class);
        Bundle b = getIntent().getExtras();
        String username = b.getString("userid");
        click.putExtra("userid", username);
        String selectedmachinename = StringList.get((int) id);
        machineID = getMachineID(selectedmachinename);
        click.putExtra("machineid", machineID);
        startActivity(click);

    }

    protected String getMachineID(String selectedmachinename){
        ConnectionClass conn = new ConnectionClass();
        String machineid = "";
        try {
            Bundle b = getIntent().getExtras();
            String user_name = b.getString("userid");
            machineid =  conn.getDeviceID(user_name, selectedmachinename);
        }
        catch(Exception ex){
            //handle javasqlexception
        }
        return machineid;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //TODO - assign a list of all the gaming entries retrieved from database to gamingEntryList

        //TODO: delete events that occur seven days prior to the current date

        ConnectionClass conn = new ConnectionClass();
        try {
            Bundle b = getIntent().getExtras();
            String user_name = b.getString("userid");
            StringList = conn.getDevices(userid);
        }
        catch(Exception ex){
            //handle javasqlexception
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, StringList);
        listview = (ListView) findViewById(R.id.device_list);

        listview.setAdapter(adapter);
    }
}
