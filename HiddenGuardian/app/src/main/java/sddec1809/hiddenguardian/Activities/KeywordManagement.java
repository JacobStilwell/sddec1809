package sddec1809.hiddenguardian.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.security.Key;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import sddec1809.hiddenguardian.R;

import static java.lang.Thread.sleep;

public class KeywordManagement extends AppCompatActivity{
    ConnectionClass connectionClass;
    EditText keyword_text;
    Button keyword_add, keyword_back;
    String original;
    ListView myListView;
    private MyListAdapter myListAdapter;
    private ArrayList<String> data = new ArrayList<String>();

    @Override
    protected  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.keyword_list);
        connectionClass = new ConnectionClass();

        keyword_text = findViewById(R.id.keyword_name);
        keyword_add = findViewById(R.id.button_add);
        keyword_back = findViewById(R.id.button_key_back);

        ListView myListView1 = findViewById(R.id.list_keywords);
        myListView=myListView1;

        generateListContent();
        myListAdapter = new MyListAdapter(this, R.layout.keyword_item, data);
        myListAdapter.notifyDataSetChanged();
        myListView.setAdapter(myListAdapter);

        keyword_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( KeywordManagement.this,MainPage.class);

                Bundle b = getIntent().getExtras();
                String user_name = b.getString("userid");
                String machineid= b.getString("machineid");
                intent.putExtra("userid", user_name);
                intent.putExtra("machineid",machineid);
                startActivity(intent);
            }
        });

        keyword_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           //     Toast.makeText(KeywordManagement.this, keyword_text.getText().toString(), Toast.LENGTH_SHORT).show();

//                CompletableFuture<Void> future = CompletableFuture.runAsync(new Runnable() {
//                    @Override
//                    public void run() {
//                        // Simulate a long-running Job
//                        try {
//                            TimeUnit.SECONDS.sleep(15000);
//                        } catch (InterruptedException e) {
//                            throw new IllegalStateException(e);
//                            Log.d("sleep", "onClick: 1000ms");
//                        }
//                    }
//                });
//
//// Block and wait for the future to complete
//                future.get()

                AddKeyword addkeyword = new AddKeyword();
                keyword_text.setText("");
                addkeyword.execute("");
                generateListContent();
                try{
                    sleep(10000);
                }catch (Exception e){
                    Log.d("sleep", "onClick: 1000ms");
                }


            }
        });
    }

    public class AddKeyword extends AsyncTask<String,String,String>
    {

        //todo add preexecute and postexecute
        String z = "";
        Boolean isSuccess = false;

        Bundle b = getIntent().getExtras();
        String userid = b.getString("userid");
        String keyword = keyword_text.getText().toString();


        @Override
        protected void onPostExecute(String result)
        {
            myListAdapter.notifyDataSetChanged();
            myListView.setAdapter(myListAdapter);
        }
        @Override
        protected String doInBackground(String... params) {

            if (keyword.trim().equals("")) {
                z = "Please enter a Keyword";
            }
            else
            {
                try {
                    Connection con = connectionClass.CONN();
                    if (con == null) {
                        z = "Error in connection with SQL server";
                    } else {
                        String query = "INSERT INTO Keywords (userid, word) SELECT '" + userid + "', '" + keyword + "' WHERE NOT EXISTS(SELECT * FROM Keywords WHERE userid = '" + userid + "' AND word = '" + keyword + "');";
                        Statement stmt = con.createStatement();
                        ResultSet rs = stmt.executeQuery(query);

                        if(rs.next())
                        {

                            z = "Keyword added";
                            isSuccess=true;
                            Toast.makeText(KeywordManagement.this, "keyword added", Toast.LENGTH_SHORT).show();

                        }
                        else
                        {
                            z = "Unable to add keyword";
                            isSuccess = false;
                            Toast.makeText(KeywordManagement.this, "keyword adding failed", Toast.LENGTH_SHORT).show();

                        }
                        con.close();
                    }
                }
                catch (Exception ex)
                {
                    isSuccess = false;
                    z = "Exceptions";
                }
            }
            return z;
        }
    }
    private void generateListContent() {
        String z = "";
        Boolean isSuccess = false;
        data.clear();
        Bundle b = getIntent().getExtras();
        String userid = b.getString("userid");
        try {
            Connection con = connectionClass.CONN();
            if (con == null) {
                z = "Error in connection with SQL server";
            } else {
                String query = "SELECT * FROM Keywords WHERE userid = '" + userid + "';";
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(query);

                if(rs.next())
                {
                    do{
                        data.add(rs.getString("word"));

                    }while(rs.next());
                    myListAdapter.notifyDataSetChanged();
                    z = "Keyword added";
                    isSuccess=true;
                }
                else
                {
                    z = "Unable to add keyword";
                    isSuccess = false;
                }
                con.close();
            }
        }
        catch (Exception ex)
        {
            isSuccess = false;
            z = "Exceptions";
        }
        myListView.setAdapter(new MyListAdapter(this,R.layout.keyword_item, data));
    }
    private class MyListAdapter extends ArrayAdapter<String> {
        private int layout;
        private MyListAdapter(Context context, int resource,  List<String> objects) {
            super(context, resource, objects);

            layout = resource;
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position,  View convertView,  ViewGroup parent) {
            KeywordEntity mainKeywordEntity = null;

            if(convertView == null){
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(layout, parent, false);
                final KeywordEntity keywordEntity = new KeywordEntity();
                keywordEntity.keyword=  convertView.findViewById(R.id.Keywordname);
                keywordEntity.delete=  convertView.findViewById(R.id.buttonDelete);

                keywordEntity.delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        DeleteKeyword deleteKeyword = new DeleteKeyword();
                        deleteKeyword.execute(keywordEntity.keyword.getText().toString());
                        notifyDataSetChanged();
                    }
                });
                convertView.setTag(keywordEntity);
            }
            mainKeywordEntity = (KeywordEntity) convertView.getTag();
            mainKeywordEntity.keyword.setText(getItem(position));
            notifyDataSetChanged();
            return convertView;
        }
        public class DeleteKeyword extends AsyncTask<String,String,String>
        {

            //todo add preexecute and postexecute
            String z = "";
            Boolean isSuccess = false;

            Bundle b = getIntent().getExtras();
            String userid = b.getString("userid");
            @Override
            protected String doInBackground(String... params) {
                String keyword = params[0];
               // Log.d("keyword", "doInBackground: "+keyword.toString());
                //Toast.makeText(KeywordManagement.this, keyword, Toast.LENGTH_SHORT).show();
                if (keyword.trim().equals("")) {
                    z = "Please enter a Keyword";
                }
                else
                {
                    try {
                        Connection con = connectionClass.CONN();
                        if (con == null) {
                            z = "Error in connection with SQL server";
                        } else {
                            String query = "DELETE FROM Keywords WHERE userid = '" + userid + "' AND word = '" + keyword + "';";
                            Statement stmt = con.createStatement();
                            ResultSet rs = stmt.executeQuery(query);

                            if(rs.next())
                            {
                                z = "Keyword was updated";
                                isSuccess=true;
                            }
                            else
                            {
                                z = "Unable to update keyword";
                                isSuccess = false;
                            }
                        }
                        con.close();
                    }
                    catch (Exception ex)
                    {
                        isSuccess = false;
                        z = "Exceptions";
                    }
                }
                 return z;
            }
            @Override
            protected void onPostExecute(String result)
            {
                generateListContent();
                myListAdapter.notifyDataSetChanged();
                myListView.invalidate();
            }

        }
    }
    public class KeywordEntity {
        TextView keyword;

        Button delete;
    }
}
