package sddec1809.hiddenguardian.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ContextMenu;
import android.view.MenuInflater;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import sddec1809.hiddenguardian.Models.GamingEntry;
import sddec1809.hiddenguardian.R;

public class MainPage extends AppCompatActivity {

    Button btnkeyword;
    ArrayList<GamingEntry> gamingEntryList;
    ListView listview;
    final Handler handler = new Handler();

    String machineid, username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page);

        btnkeyword = findViewById(R.id.button_Keyword);
        listview = findViewById(R.id.list);
        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        Bundle b = getIntent().getExtras();
        username = b.getString("userid");
        machineid = b.getString("machineid");

        new Timer().scheduleAtFixedRate(new update(),0,60000);


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("onitemtag", "onitemclick");
                gotoListDetails(id);
            }
        });


        btnkeyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainPage.this, KeywordManagement.class);

                Bundle b = getIntent().getExtras();
                String user_name = b.getString("userid");
                String machineid= b.getString("machineid");
                intent.putExtra("userid", user_name);
                intent.putExtra("machineid", machineid);

                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater mMenuInflater = getMenuInflater();
        mMenuInflater.inflate(R.menu.hg_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.logout){
            Intent click = new Intent(this,MainActivity.class);
            startActivity(click);
        }
        if(item.getItemId() == R.id.add_device){
            Intent click = new Intent(this,AddDevice.class);
            Bundle b = getIntent().getExtras();
            String username = b.getString("userid");
            click.putExtra("userid", username);
            startActivity(click);
        }
        if(item.getItemId() == R.id.switch_device){
            Intent click = new Intent(this,ShowDevices.class);
            Bundle b = getIntent().getExtras();
            String username = b.getString("userid");
            click.putExtra("userid", username);
            startActivity(click);
        }
        return true;
    }
    protected void gotoListDetails(long id){
        Intent click = new Intent(this,GamingSessionPage.class);
        click.putExtra("userid", username);
        click.putExtra("machineid", machineid);
        click.putExtra("session_id", gamingEntryList.get((int) id).getSessionid());
        startActivity(click);
    }

   public class update extends TimerTask {
        @Override
        public void run() {
            handler.post(new Runnable() {
                public void run() {

                    ConnectionClass conn = new ConnectionClass();
                    try {
                        ArrayList<GamingEntry> temp = conn.UpdateDB(machineid);
                        gamingEntryList = updategamingentry(temp);

                    } catch (Exception ex) {
                        //handle javasqlexception
                    }
                    listview = (ListView) findViewById(R.id.list);
                    ArrayAdapter<GamingEntry> adapter = updategamingEntryList(gamingEntryList);
                    listview.setAdapter(adapter);
                    //adapter.notifyDataSetChanged();
                }
            });
        }
    }


    public ArrayList<GamingEntry> updategamingentry(ArrayList<GamingEntry> ge) throws SQLException {
        ArrayList<GamingEntry> ge2 = new ArrayList<GamingEntry>();
        ArrayList<Integer> sessionlist = new ArrayList<Integer>();
        ArrayList<Integer> keywordcount = new ArrayList<Integer>();
        ArrayList<String> datelist = new ArrayList<String>();
        for (int i = 0; i < ge.size(); ++i) {
            GamingEntry ge1 = ge.get(i);
            if (sessionlist.contains(ge1.getSessionid())){
                if (keywordcount.get(sessionlist.indexOf(ge1.getSessionid())).equals(null)){
                    keywordcount.add(sessionlist.indexOf(ge1.getSessionid()), ge1.getKeywordnumber());
                }
                else{
                    keywordcount.set(sessionlist.indexOf(ge1.getSessionid()), ge1.getKeywordnumber() + keywordcount.get(sessionlist.indexOf(ge1.getSessionid())));
                }
            }
            else{
                sessionlist.add(ge1.getSessionid());
                keywordcount.add(ge1.getKeywordnumber());
                datelist.add(ge1.getdate());
            }
        }
        for (int j = 0; j < sessionlist.size(); j++){
            GamingEntry g  = new GamingEntry(ge.get(0).getUserid(), ge.get(0).getMachineid(), sessionlist.get(j), datelist.get(j), keywordcount.get(j));
            g.setTitle(datelist.get(j), keywordcount.get(j));
            ge2.add(j, g);
        }
        return ge2;
    }

    public ArrayAdapter<GamingEntry> updategamingEntryList(ArrayList<GamingEntry> geList){
        ArrayAdapter<GamingEntry> adapter = new ArrayAdapter<GamingEntry>(this,
                android.R.layout.simple_list_item_1, gamingEntryList);
        return adapter;
    }


   @Override
    protected void onResume() {
        super.onResume();
        //TODO - assign a list of all the gaming entries retrieved from database to gamingEntryList

        //TODO: delete events that occur seven days prior to the current date

        ConnectionClass conn = new ConnectionClass();
        try {
        Bundle b = getIntent().getExtras();
        ArrayList<GamingEntry> temp = conn.UpdateDB(machineid);
        gamingEntryList = updategamingentry(temp);
    }
        catch(Exception ex){
        //handle javasqlexception
    }
    ArrayAdapter<GamingEntry> adapter = new ArrayAdapter<GamingEntry>(this,
            android.R.layout.simple_list_item_activated_1, gamingEntryList);
    listview = (ListView) findViewById(R.id.list);
        listview.setAdapter(adapter);
}
}

