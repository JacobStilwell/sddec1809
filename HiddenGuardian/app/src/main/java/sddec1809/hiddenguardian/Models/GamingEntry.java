package sddec1809.hiddenguardian.Models;


import sddec1809.hiddenguardian.Activities.ConnectionClass;


public class GamingEntry {
    private String keywords;
    private String confidence_rating;
    private String date;
    private String title;
    private int keywordnumber;
    private int sessionid;
    private String message;
    private String userid;
    private String machineid;
    ConnectionClass connectionClass;

    public GamingEntry(String userid, String machineid, int sessionid, String date, int keywordnumber ){
        this.userid = userid;
        this.machineid = machineid;
        this.date=date;
        this.keywordnumber = keywordnumber;
        this.sessionid = sessionid;
    }

    public GamingEntry(){}

    public String getKeywords(){
            ConnectionClass connectionClass = new ConnectionClass();
            return connectionClass.getKeywords(getUserid(), getMessage());

    };
    public String getConfidence_rating(){ return confidence_rating;};
    public String[] checkMessage(){
        //create a method for this because it's used twice
        String[] keywordArray = keywords.split(",");
        String[] keywordTrigger = new String[50];
        int i = 0;
        int j = 0;
        while (i < keywordArray.length) {
            String[] msgarray = message.toLowerCase().split(" ");
            for (int k = 0; k < msgarray.length; ++k) {
                if (msgarray[k].equals(keywordArray[i])){
                    keywordTrigger[j] = keywordArray[i];
                    ++j;
                }
            }
            ++i;
        }
        setKeywordTriggers(j);
        return keywordTrigger;
    }

    public String getdate(){return date;}
    public String getMessage(){return message;}
    public String getUserid(){return userid;}
    public int getSessionid(){return sessionid;}
    public String getMachineid(){return machineid;}
    public int getKeywordnumber(){
        return keywordnumber;
    }
    public void setKeywordTriggers(int j){this.keywordnumber = j;};
    public void setMessage(String message){this.message = message;};
    public void setsessionid(int sessionid){this.sessionid = sessionid;};
    public void setConfidence_rating(String confidence_rating){this.confidence_rating=confidence_rating;};
    public void setuserid(String userid){this.userid=userid;};
    public void setKeywords(String keywords){this.keywords=keywords;};
    public void setDate(String date){this.date=date;};
    public void setmachineid(String machineid){this.machineid=machineid;};
    public void setTitle(String date, int keywordnumber) throws java.sql.SQLException{this.title = "Gaming Session " + sessionid + "\nDate: " + date + "\nKeyword(s) Triggered: " + keywordnumber;}

    @Override
    public String toString() {
        return this.title;
    }
}

