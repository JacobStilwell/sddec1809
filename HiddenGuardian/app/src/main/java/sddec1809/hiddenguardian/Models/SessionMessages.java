package sddec1809.hiddenguardian.Models;

import android.text.Spannable;
import android.text.SpannableStringBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import sddec1809.hiddenguardian.Activities.ConnectionClass;
import java.util.List;
import java.util.Arrays;

public class SessionMessages{
    private String keywords;
    private String confidence_rating;
    private String date;
    private String title;
    private int keywordnumber;
    private int sessionid;
    private int messageid;
    private String message;
    private String userid;
    private int sessionKeywordNumber;
    private String timestamp;
    ConnectionClass connectionClass;

    public SessionMessages(String userid, String date, String message, String confidence){
        this.userid = userid;
        this.date=date;
        this.message = message;
        this.confidence_rating = confidence;
        this.sessionid = sessionid;
        this.messageid = messageid;
    }

    public SessionMessages(){}

    public String getConfidence_rating(){ return confidence_rating;};
    public String checkMessage(){
        //create a method for this because it's used twice
        String[] keywordArray = keywords.split(",");
        String keywordTrigger = "";
        int i = 0;
        int j = 0;
        while (i < keywordArray.length) {
            String[] msgarray = message.toLowerCase().split(" ");
            for (int k = 0; k < msgarray.length; ++k) {
                if (msgarray[k].equals(keywordArray[i])){
                    keywordTrigger += keywordArray[i] + ", ";
                    ++j;
                }
            }
            ++i;
        }
        setKeywordNumber(j);
        return keywordTrigger;
    }

    public String getdate(){return date;}
    public String getMessage(){return message;}
    public String getUserid(){return userid;}
    public int getSessionid(){return sessionid;}
    public int getMessageid(){return messageid;}
    public int getKeywordnumber(){
        return keywordnumber;
    }
    public void setKeywordNumber(int j){this.keywordnumber = j;};
    public void setMessage(String message){this.message = message;};
    public void setSessionKeywordNumber(){this.sessionKeywordNumber = sessionKeywordNumber;};
    public void setConfidence_rating(String confidence_rating){this.confidence_rating=confidence_rating;};
    public void setuserid(String userid){this.userid=userid;};
    public void setKeywords(String keywords){this.keywords=keywords;};
    public void setDate(String date){this.date=date;};
    public void setMessageID(int messageid){this.messageid = messageid;};
    public void setSessionID(int sessionid) {this.sessionid = sessionid;};
    public void setTimeStamp(String timestamp){this.timestamp = timestamp;};
    public void setTitle(String keywordstriggered) throws java.sql.SQLException{
       this.title = "TimeStamp: " + timestamp + "\n" + "Keyword(s) Spoken: " + keywordstriggered;
//        SpannableStringBuilder str = new SpannableStringBuilder(title_1);
//        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 0, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 2, 3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//        this.title = str;


        ;}

    @Override
    public String toString() {
        return this.title;
    }
}
