package netid.iastate.edu.lab5.Models;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public class EventDao_Impl implements EventDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfEvent;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfEvent;

  public EventDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfEvent = new EntityInsertionAdapter<Event>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `event`(`uid`,`title`,`location`,`start_time`,`end_time`,`details`) VALUES (nullif(?, 0),?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Event value) {
        stmt.bindLong(1, value.getUid());
        if (value.getTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getTitle());
        }
        if (value.getLocation() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getLocation());
        }
        if (value.getStartTime() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getStartTime());
        }
        if (value.getEndTime() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getEndTime());
        }
        if (value.getDetails() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getDetails());
        }
      }
    };
    this.__deletionAdapterOfEvent = new EntityDeletionOrUpdateAdapter<Event>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `event` WHERE `uid` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Event value) {
        stmt.bindLong(1, value.getUid());
      }
    };
  }

  @Override
  public void insertEvent(Event event) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfEvent.insert(event);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(Event event) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfEvent.handle(event);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<Event> getAll() {
    final String _sql = "SELECT * FROM event";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfUid = _cursor.getColumnIndexOrThrow("uid");
      final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
      final int _cursorIndexOfLocation = _cursor.getColumnIndexOrThrow("location");
      final int _cursorIndexOfStartTime = _cursor.getColumnIndexOrThrow("start_time");
      final int _cursorIndexOfEndTime = _cursor.getColumnIndexOrThrow("end_time");
      final int _cursorIndexOfDetails = _cursor.getColumnIndexOrThrow("details");
      final List<Event> _result = new ArrayList<Event>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Event _item;
        _item = new Event();
        final long _tmpUid;
        _tmpUid = _cursor.getLong(_cursorIndexOfUid);
        _item.setUid(_tmpUid);
        final String _tmpTitle;
        _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
        _item.setTitle(_tmpTitle);
        final String _tmpLocation;
        _tmpLocation = _cursor.getString(_cursorIndexOfLocation);
        _item.setLocation(_tmpLocation);
        final String _tmpStartTime;
        _tmpStartTime = _cursor.getString(_cursorIndexOfStartTime);
        _item.setStartTime(_tmpStartTime);
        final String _tmpEndTime;
        _tmpEndTime = _cursor.getString(_cursorIndexOfEndTime);
        _item.setEndTime(_tmpEndTime);
        final String _tmpDetails;
        _tmpDetails = _cursor.getString(_cursorIndexOfDetails);
        _item.setDetails(_tmpDetails);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Event findByID(long id) {
    final String _sql = "SELECT * FROM event WHERE uid IN (?)";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfUid = _cursor.getColumnIndexOrThrow("uid");
      final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
      final int _cursorIndexOfLocation = _cursor.getColumnIndexOrThrow("location");
      final int _cursorIndexOfStartTime = _cursor.getColumnIndexOrThrow("start_time");
      final int _cursorIndexOfEndTime = _cursor.getColumnIndexOrThrow("end_time");
      final int _cursorIndexOfDetails = _cursor.getColumnIndexOrThrow("details");
      final Event _result;
      if(_cursor.moveToFirst()) {
        _result = new Event();
        final long _tmpUid;
        _tmpUid = _cursor.getLong(_cursorIndexOfUid);
        _result.setUid(_tmpUid);
        final String _tmpTitle;
        _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
        _result.setTitle(_tmpTitle);
        final String _tmpLocation;
        _tmpLocation = _cursor.getString(_cursorIndexOfLocation);
        _result.setLocation(_tmpLocation);
        final String _tmpStartTime;
        _tmpStartTime = _cursor.getString(_cursorIndexOfStartTime);
        _result.setStartTime(_tmpStartTime);
        final String _tmpEndTime;
        _tmpEndTime = _cursor.getString(_cursorIndexOfEndTime);
        _result.setEndTime(_tmpEndTime);
        final String _tmpDetails;
        _tmpDetails = _cursor.getString(_cursorIndexOfDetails);
        _result.setDetails(_tmpDetails);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
