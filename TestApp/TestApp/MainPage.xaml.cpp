﻿//
// MainPage.xaml.cpp
// Implementation of the MainPage class.
//

#include "pch.h"
#include "MainPage.xaml.h"
#include "AudioPermissions.h"


using namespace TestApp;
using namespace Concurrency;
using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::Globalization;
using namespace Windows::Media::SpeechRecognition;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Automation::Peers;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Documents;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Interop;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

MainPage::MainPage()
{
	InitializeComponent();
}

void MainPage::NotifyUser(String^ strMessage, NotifyType type)
{
	if (Dispatcher->HasThreadAccess)
	{
		UpdateStatus(strMessage, type);
	}
	else
	{
		Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler([strMessage, type, this]()
		{
			UpdateStatus(strMessage, type);
		}));
	}
}

void MainPage::UpdateStatus(String^ strMessage, NotifyType type)
{
	switch (type)
	{
	case NotifyType::StatusMessage:
		StatusBorder->Background = ref new SolidColorBrush(Windows::UI::Colors::Green);
		break;
	case NotifyType::ErrorMessage:
		StatusBorder->Background = ref new SolidColorBrush(Windows::UI::Colors::Red);
		break;
	default:
		break;
	}

	StatusBlock->Text = strMessage;

	// Collapse the StatusBlock if it has no text to conserve real estate.
	//if (StatusBlock->Text != "")
	//{
		//StatusBorder->Visibility = Windows::UI::Xaml::Visibility::Visible;
		//StatusPanel->Visibility = Windows::UI::Xaml::Visibility::Visible;
	//}
	//else
	//{
		//StatusBorder->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
		//StatusPanel->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
	//}

	// Raise an event if necessary to enable a screen reader to announce the status update.
	auto peer = dynamic_cast<FrameworkElementAutomationPeer^>(FrameworkElementAutomationPeer::FromElement(StatusBlock));
	if (peer != nullptr)
	{
		peer->RaiseAutomationEvent(AutomationEvents::LiveRegionChanged);
	}
}

void MainPage::InitializeRecognizer(Windows::Globalization::Language^ recognizerLanguage)
{
	// If reinitializing the recognizer (ie, changing the speech language), clean up the old recognizer first.
	// Avoid doing this while the recognizer is active by disabling the ability to change languages while listening.
	if (this->speechRecognizer != nullptr)
	{
		speechRecognizer->StateChanged -= stateChangedToken;
		speechRecognizer->ContinuousRecognitionSession->Completed -= continuousRecognitionCompletedToken;
		speechRecognizer->ContinuousRecognitionSession->ResultGenerated -= continuousRecognitionResultGeneratedToken;
		speechRecognizer->HypothesisGenerated -= hypothesisGeneratedToken;

		delete this->speechRecognizer;
		this->speechRecognizer = nullptr;
	}

	this->speechRecognizer = ref new SpeechRecognizer(recognizerLanguage);

	// Provide feedback to the user about the state of the recognizer. This can be used to provide visual feedback in the form
	// of an audio indicator to help the user understand whether they're being heard.
	stateChangedToken = speechRecognizer->StateChanged +=
		ref new TypedEventHandler<
		SpeechRecognizer ^,
		SpeechRecognizerStateChangedEventArgs ^>(
			this,
			&MainPage::SpeechRecognizer_StateChanged);

	// Apply the dictation topic constraint to optimize for dictated freeform speech.
	auto dictationConstraint = ref new SpeechRecognitionTopicConstraint(SpeechRecognitionScenario::Dictation, "dictation");
	speechRecognizer->Constraints->Append(dictationConstraint);

	create_task(speechRecognizer->CompileConstraintsAsync(), task_continuation_context::use_current())
		.then([this](task<SpeechRecognitionCompilationResult^> previousTask)
	{
		SpeechRecognitionCompilationResult^ compilationResult = previousTask.get();

		if (compilationResult->Status != SpeechRecognitionResultStatus::Success)
		{
			btnContinuousRecognize->IsEnabled = false;

			// Let the user know that the grammar didn't compile properly.
			this->NotifyUser("Grammar Compilation Failed: " + compilationResult->Status.ToString(), NotifyType::ErrorMessage);
		}

		// Handle continuous recognition events. Completed fires when various error states occur. ResultGenerated fires when
		// some recognized phrases occur, or the garbage rule is hit. HypothesisGenerated fires during recognition, and
		// allows us to provide incremental feedback based on what the user's currently saying.
		continuousRecognitionCompletedToken = speechRecognizer->ContinuousRecognitionSession->Completed +=
			ref new TypedEventHandler<
			SpeechContinuousRecognitionSession ^,
			SpeechContinuousRecognitionCompletedEventArgs ^>(
				this,
				&MainPage::ContinuousRecognitionSession_Completed);
		continuousRecognitionResultGeneratedToken = speechRecognizer->ContinuousRecognitionSession->ResultGenerated +=
			ref new TypedEventHandler<
			SpeechContinuousRecognitionSession ^,
			SpeechContinuousRecognitionResultGeneratedEventArgs ^>(
				this,
				&MainPage::ContinuousRecognitionSession_ResultGenerated);
		hypothesisGeneratedToken = speechRecognizer->HypothesisGenerated +=
			ref new TypedEventHandler<
			SpeechRecognizer ^,
			SpeechRecognitionHypothesisGeneratedEventArgs ^>(
				this,
				&MainPage::SpeechRecognizer_HypothesisGenerated);


	}, task_continuation_context::use_current());
}

void MainPage::ContinuousRecognize_Click(Object^ sender, RoutedEventArgs^ e)
{
	btnContinuousRecognize->IsEnabled = false;
	// The recognizer can only start listening in a continuous fashion if the recognizer is currently idle.
	// This prevents an exception from occurring.
	if (speechRecognizer->State == SpeechRecognizerState::Idle)
	{
		DictationButtonText->Text = " Stop Dictation";
		//cbLanguageSelection->IsEnabled = false;
		//hlOpenPrivacySettings->Visibility = Windows::UI::Xaml::Visibility::Collapsed;
		discardedTextBlock->Visibility = Windows::UI::Xaml::Visibility::Collapsed;

		try
		{
			create_task(speechRecognizer->ContinuousRecognitionSession->StartAsync(), task_continuation_context::use_current())
				.then([this](task<void> startAsyncTask)
			{
				try
				{
					// Retreive any exceptions that may have been generated by StartAsync.
					startAsyncTask.get();
				}
				catch (Exception^ exception)
				{
					DictationButtonText->Text = " Dictate";
					//cbLanguageSelection->IsEnabled = true;

					auto messageDialog = ref new Windows::UI::Popups::MessageDialog(exception->Message, "Exception");
					create_task(messageDialog->ShowAsync());
				}
			}).then([this]() {
				btnContinuousRecognize->IsEnabled = true;
			});
		}
		catch (COMException^ exception)
		{
			if ((unsigned int)exception->HResult == HResultPrivacyStatementDeclined)
			{
				// If the privacy policy hasn't been accepted, attempting to create a task out of RecognizeAsync fails immediately with a
				// COMException and this specific HResult.
				//hlOpenPrivacySettings->Visibility = Windows::UI::Xaml::Visibility::Visible;
			}
			else
			{
				auto messageDialog = ref new Windows::UI::Popups::MessageDialog(exception->Message, "Exception");
				create_task(messageDialog->ShowAsync());
			}
		}
	}
	else
	{
		DictationButtonText->Text = " Dictate";
		//cbLanguageSelection->IsEnabled = true;


		// Cancelling recognition prevents any currently recognized speech from
		// generating a ResultGenerated event. StopAsync() will allow the final session to 
		// complete.
		create_task(speechRecognizer->ContinuousRecognitionSession->StopAsync(), task_continuation_context::use_current())
			.then([this]()
		{

			// Ensure we don't leave any hypothesis text behind
			dictationTextBox->Text = ref new Platform::String(this->dictatedTextBuilder.str().c_str());
		}).then([this]() {
			btnContinuousRecognize->IsEnabled = true;
		});
	}
}

void MainPage::btnClearText_Click(Object^ sender, RoutedEventArgs^ e)
{
	btnClearText->IsEnabled = false;
	this->dictatedTextBuilder.str(L"");
	this->dictatedTextBuilder.clear();
	dictationTextBox->Text = "";
	discardedTextBlock->Visibility = Windows::UI::Xaml::Visibility::Collapsed;

	btnContinuousRecognize->Focus(Windows::UI::Xaml::FocusState::Programmatic);
}

void MainPage::SpeechRecognizer_StateChanged(SpeechRecognizer ^sender, SpeechRecognizerStateChangedEventArgs ^args)
{
	Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler([this, args]()
	{
		this->NotifyUser("Speech recognizer state: " + args->State.ToString(), NotifyType::StatusMessage);
	}));

}

void MainPage::ContinuousRecognitionSession_Completed(SpeechContinuousRecognitionSession ^sender, SpeechContinuousRecognitionCompletedEventArgs ^args)
{
	if (args->Status != SpeechRecognitionResultStatus::Success)
	{
		// If TimeoutExceeded occurs, the user has been silent for too long. We can use this to 
		// cancel recognition if the user in dictation mode and walks away from their device, etc.
		// In a global-command type scenario, this timeout won't apply automatically.
		// With dictation (no grammar in place) modes, the default timeout is 20 seconds.
		if (args->Status == SpeechRecognitionResultStatus::TimeoutExceeded)
		{
			Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler([this, args]()
			{
				this->NotifyUser("Automatic Time Out of Dictation", NotifyType::StatusMessage);
				DictationButtonText->Text = " Dictate";
				//cbLanguageSelection->IsEnabled = true;
				dictationTextBox->Text = ref new Platform::String(this->dictatedTextBuilder.str().c_str());
			}));
		}
		else
		{
			Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler([this, args]()
			{
				this->NotifyUser("Continuous Recognition Completed: " + args->Status.ToString(), NotifyType::ErrorMessage);
				DictationButtonText->Text = " Dictate";
				//cbLanguageSelection->IsEnabled = true;
			}));
		}
	}
}

void MainPage::SpeechRecognizer_HypothesisGenerated(SpeechRecognizer ^sender, SpeechRecognitionHypothesisGeneratedEventArgs ^args)
{
	String^ hypothesis = args->Hypothesis->Text;

	std::wstring textBoxContent = dictatedTextBuilder.str() + L" " + hypothesis->Data() + L"...";
	Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler([this, textBoxContent]()
	{
		// Update the textbox with the currently confirmed text, and the hypothesis combined.
		this->dictationTextBox->Text = ref new Platform::String(textBoxContent.c_str());
		this->btnClearText->IsEnabled = true;
	}));
}

void MainPage::ContinuousRecognitionSession_ResultGenerated(SpeechContinuousRecognitionSession ^sender, SpeechContinuousRecognitionResultGeneratedEventArgs ^args)
{
	//std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	//std::time_t tt;
	//tt = std::chrono::system_clock::to_time_t(now);
	//char str[26];
	//ctime_s(str, sizeof str, &tt);
	//this->dictatedTextBuilder << str;
	// We may choose to discard content that has low confidence, as that could indicate that we're picking up
	// noise via the microphone, or someone could be talking out of earshot.
	if (args->Result->Confidence == SpeechRecognitionConfidence::High)
	{
		this->dictatedTextBuilder << args->Result->Text->Data() << " \n";

	}
	else if (args->Result->Confidence == SpeechRecognitionConfidence::Medium)
	{
		this->dictatedTextBuilder << "<y>" << args->Result->Text->Data() << "</y> \n";
	}
	else
	{
		this->dictatedTextBuilder << "<r>" << args->Result->Text->Data() << "</r> \n";
	}

	Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler([this]()
	{
		this->discardedTextBlock->Visibility = Windows::UI::Xaml::Visibility::Collapsed;

		this->dictationTextBox->Text = ref new String(this->dictatedTextBuilder.str().c_str());
		this->btnClearText->IsEnabled = true;
	}));

}


void MainPage::dictationTextBox_TextChanged(Object^ sender, TextChangedEventArgs^ e)
{
	auto grid = static_cast<Grid^>(VisualTreeHelper::GetChild(dictationTextBox, 0));
	for (int i = 0; i < VisualTreeHelper::GetChildrenCount(grid) - 1; i++)
	{
		ScrollViewer^ scroller = dynamic_cast<ScrollViewer^>(VisualTreeHelper::GetChild(grid, i));
		if (scroller != nullptr)
		{
			scroller->ChangeView(0.0, scroller->ExtentHeight, 1.0f);
			break;
		}
	}
}

void MainPage::OnNavigatedTo(NavigationEventArgs^ e)
{
	Page::OnNavigatedTo(e);

	// Prompt the user for permission to access the microphone. This request will only happen
	// once, it will not re-prompt if the user rejects the permission.
	create_task(AudioPermissions::RequestMicrophonePermissionAsync(), task_continuation_context::use_current())
		.then([this](bool permissionGained)
	{
		if (permissionGained)
		{
		this->btnContinuousRecognize->IsEnabled = true;

		InitializeRecognizer(SpeechRecognizer::SystemSpeechLanguage);
		}
		 else
		 {
		    this->dictationTextBox->Text = "Permission to access capture resources was not given by the user; please set the application setting in Settings->Privacy->Microphone.";
		    //this->cbLanguageSelection->IsEnabled = false;
		 }
	});

	

	
}

void MainPage::OnNavigatedFrom(NavigationEventArgs^ e)
{
	Page::OnNavigatedFrom(e);

	if (speechRecognizer != nullptr)
	{
		// If we're currently active, start a cancellation task, and wait for it to finish before shutting down
		// the recognizer.
		Concurrency::task<void> cleanupTask;
		if (speechRecognizer->State != SpeechRecognizerState::Idle)
		{
			cleanupTask = create_task(speechRecognizer->ContinuousRecognitionSession->CancelAsync(), task_continuation_context::use_current());
		}
		else
		{
			cleanupTask = create_task([]() {}, task_continuation_context::use_current());
		}

		cleanupTask.then([this]() {
			DictationButtonText->Text = " Dictate";
			dictationTextBox->Text = "";

			speechRecognizer->StateChanged -= stateChangedToken;
			speechRecognizer->ContinuousRecognitionSession->Completed -= continuousRecognitionCompletedToken;
			speechRecognizer->ContinuousRecognitionSession->ResultGenerated -= continuousRecognitionResultGeneratedToken;
			speechRecognizer->HypothesisGenerated -= hypothesisGeneratedToken;

			delete speechRecognizer;
			speechRecognizer = nullptr;
		}, task_continuation_context::use_current());
	}
}