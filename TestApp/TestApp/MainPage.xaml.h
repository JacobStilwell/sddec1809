﻿//
// MainPage.xaml.h
// Declaration of the MainPage class.
//

#pragma once

#include "MainPage.g.h"
#include <sstream>
#include <chrono>
#include <ctime>

namespace TestApp
{

	public enum class NotifyType
	{
		StatusMessage,
		ErrorMessage
	};

	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public ref class MainPage sealed
	{
	public:
		MainPage();

	protected:
		virtual void OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override;
		virtual void OnNavigatedFrom(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override;

	private:
		static const unsigned int HResultPrivacyStatementDeclined = 0x80045509;


		Windows::Media::SpeechRecognition::SpeechRecognizer^ speechRecognizer;
		std::wstringstream dictatedTextBuilder;

		void ContinuousRecognize_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void btnClearText_Click(Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e);
		void dictationTextBox_TextChanged(Platform::Object^ sender, Windows::UI::Xaml::Controls::TextChangedEventArgs^ e);
		void UpdateStatus(Platform::String^ strMessage, NotifyType type);
		void InitializeRecognizer(Windows::Globalization::Language^ recognizerLanguage);

		Windows::Foundation::EventRegistrationToken stateChangedToken;
		Windows::Foundation::EventRegistrationToken continuousRecognitionCompletedToken;
		Windows::Foundation::EventRegistrationToken continuousRecognitionResultGeneratedToken;
		Windows::Foundation::EventRegistrationToken hypothesisGeneratedToken;

		void SpeechRecognizer_StateChanged(Windows::Media::SpeechRecognition::SpeechRecognizer ^sender, Windows::Media::SpeechRecognition::SpeechRecognizerStateChangedEventArgs ^args);
		void ContinuousRecognitionSession_Completed(Windows::Media::SpeechRecognition::SpeechContinuousRecognitionSession ^sender, Windows::Media::SpeechRecognition::SpeechContinuousRecognitionCompletedEventArgs ^args);
		void ContinuousRecognitionSession_ResultGenerated(Windows::Media::SpeechRecognition::SpeechContinuousRecognitionSession ^sender, Windows::Media::SpeechRecognition::SpeechContinuousRecognitionResultGeneratedEventArgs ^args);
		void SpeechRecognizer_HypothesisGenerated(Windows::Media::SpeechRecognition::SpeechRecognizer ^sender, Windows::Media::SpeechRecognition::SpeechRecognitionHypothesisGeneratedEventArgs ^args);

	internal:
		static MainPage^ Current;
		void NotifyUser(Platform::String^ strMessage, NotifyType type);
	};
}